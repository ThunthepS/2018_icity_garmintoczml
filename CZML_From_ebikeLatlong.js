//To change before running!!
////////////////////////////////////////////////////// 
////////////////// Setting Here ////////////////// 
///////////////////////////////////////////////////
var file_in_gpx = './Data/ExportToCZML/11_24_2017_GPX_2.csv';
var file_in_tcx = './Data/ExportToCZML/11_24_2017_TCX_2.csv';
var file_outname = './Data/ExportToCZMLOut/czml_11_24_2017_GPX_2.json';
var idSuffix = "Garmin_11_24_2017_2"; // ex "Garmin_11_22_2017_2"
var textLabel = "Ebike-ID: 04\nParticipant-ID: 05";

////Imported////
//"Garmin_11_22_2017_1"
//Ebike-ID: 02
//Part-ID: 02

////Imported////
//"Garmin_11_22_2017_2"
//Ebike-ID: 04
//Part-ID: 03
///////////////////////////////////////////////////
///////////////////////////////////////////////////
var StartLine = 1;
var czml_path_width = 4;
var jsonfile = require('jsonfile');
var parse = require('csv-parse');
var fs = require('fs');
var LocationData, out, out2, LdJSON, opLength, startTime, endTime, DistanceM, Speed;
//const LastLine = 200;
fs.readFile(file_in_gpx, function (err, output) {
    if (err) throw err;
    out = output;
    parse(out, { comment: '#' }, function (err, output) {
        //console.log(output[1][1]);
        LocationData = [0];
        LocationData.push(parseFloat(output[StartLine][1]));
        LocationData.push(parseFloat(output[StartLine][0]));
        LocationData.push(parseFloat(output[StartLine][2]));
        locationDate = new Date(output[StartLine][3]);
        var tempData = [output[StartLine][3]];
        var HrData = [output[StartLine][3]];
        var RlAlt = [output[StartLine][3]];
        tempData.push(parseFloat(output[StartLine][4]));
        HrData.push(parseFloat(output[StartLine][5]));
        RlAlt.push(parseFloat(output[StartLine][2]));
        Tim = locationDate.getTime();
        opLength = output.length - 1;
        startTime = output[StartLine][3];
        endTime = output[opLength - 1][3];
        for (let i = StartLine + 1; i < output.length /*size of the csv -1*/; i++) {
            var locationDate = new Date(output[i][3]); //time
            var TimSec = locationDate.getTime();
            LocationData.push((TimSec - Tim) / 1000);
            LocationData.push(parseFloat(output[i][1])); //lon for locate
            LocationData.push(parseFloat(output[i][0])); //lat for locate
            LocationData.push(parseFloat(output[i][2])); //alt for locate
            tempData.push(output[i][3]);                 //temp property time
            tempData.push(parseFloat(output[i][4]));     //temp property for display
            HrData.push(output[i][3]);                  //HeartRate property time
            HrData.push(parseFloat(output[i][5]));       //HeartRate property for display
            RlAlt.push(output[i][3]);               //// alt property for time
            RlAlt.push(parseFloat(output[i][2])); //// alt property for display
        }

        //To Fix!!
        fs.readFile(file_in_tcx, function (err, output2) {
            if (err) throw err;
            out2 = output2;
            parse(out2, { comment: '#' }, function (err, output2) {
                var ze = 0;
                DistanceM = [output2[StartLine][0]];
                Speed = [output2[StartLine][0]];
                DistanceM.push(ze);
                Speed.push(parseFloat(output2[StartLine][2]));
                for (let i = StartLine + 1; i < output2.length /*size of the csv -1*/; i++) {
                    var DistanceM_Value = parseFloat([output2[i][1]]) - parseFloat([output2[StartLine][1]]); //time
                    DistanceM.push(output2[i][0]);    //DistanceM property time
                    DistanceM.push(DistanceM_Value);  //DistanceM for display
                    Speed.push(output2[i][0]);          //Speed property time
                    Speed.push(parseFloat(output2[i][2])); //Speed property  for display
                }


                // Build CZML with the incoming Position data

                //var pos = [0, 9.17761392, 48.7831606, 305.1999969, 1, 9.177607214, 48.78316504, 305.1999969];
                var result = [{
                    "id": "document",
                    "name": "Data" + idSuffix,
                    "version": "1.0",
                    "clock": {
                        "interval": startTime + "/" + endTime,
                        "currentTime": startTime,
                        "multiplier": 20,
                        "range": "LOOP_STOP",
                        "step": "SYSTEM_CLOCK_MULTIPLIER"
                    }
                }, {
                    "id": idSuffix,
                    "name": "ebike 1 with Garmin",
                    "availability": startTime + "/" + endTime,
                    "ellipsoid": {
                        "radii": {
                            "cartesian": [5, 5, 5]
                        },
                        "fill": true,
                        "material": {
                            "solidColor": {
                                "color": {
                                    "rgba": [255, 0, 0, 255]
                                }
                            }
                        }
                    },
                    "label": {
                        "text": textLabel,
                        "font": "14pt sans-serif",
                        "heightReference": "CLAMP_TO_GROUND",
                        "showBackground": "true",
                        "horizontalOrigin": "LEFT",
                        "verticalOrigin": "BASELINE",
                        "backgroundPadding": {
                            "cartesian2": [20, 8]
                        },
                        "pixelOffset": {
                            "cartesian2": [50, -50]
                        },
                        "backgroundColor": {
                            "rgba": [
                                0,
                                0,
                                0,
                                125
                            ]
                        },
                        "disableDepthTestDistance": 999999
                    },
                    "path": {
                        "show": [{
                            "interval": startTime + "/" + endTime,
                            "boolean": true
                        }
                        ],
                        "width": czml_path_width,
                        "material": {
                            "polylineOutline": {
                                "color": {
                                    "rgba": [0, 255, 0, 255]
                                },
                                "outlineColor": {
                                    "rgba": [0, 0, 0, 255]
                                },
                                "outlineWidth": 1
                            }
                        },
                        "resolution": 1200,
                        "leadTime": 0
                    },
                    "position": {
                        "interpolationAlgorithm": "LAGRANGE",
                        "interpolationDegree": 5,
                        "epoch": startTime,
                        "cartographicDegrees": LocationData
                    }
                }, {
                    "id": "Temperature" + idSuffix,
                    "name": "Temperature" + idSuffix,
                    "properties": {
                        "constant_property": true,
                        "TempValue": {
                            "number": tempData
                        }
                    }
                }, {
                    "id": "Heartrate" + idSuffix,
                    "name": "Heartrate" + idSuffix,
                    "properties": {
                        "constant_property": true,
                        "hrValue": {
                            "number": HrData
                        }
                    }
                }, {
                    "id": "Speed" + idSuffix,
                    "name": "Speed" + idSuffix,
                    "properties": {
                        "constant_property": true,
                        "spVal": {
                            "number": Speed
                        }
                    }
                }, {
                    "id": "DistanceM" + idSuffix,
                    "name": "DistanceM" + idSuffix,
                    "properties": {
                        "constant_property": true,
                        "DmVal": {
                            "number": DistanceM
                        }
                    }
                }, {
                    "id": "Alt" + idSuffix,
                    "name": "Alt" + idSuffix,
                    "properties": {
                        "constant_property": true,
                        "AltVal": {
                            "number": RlAlt
                        }
                    }
                }];

                jsonfile.writeFile(file_outname, result, function (err) {
                    if (err) throw err;
                    console.log('filesaved');
                })
                //}, 1000);
            });
        });

    });
});